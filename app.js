const express = require('express');
const fileUpload = require('express-fileupload');
const app = express();
var cors = require('cors')

// default options
app.use(cors())
app.use(fileUpload());

app.post('/upload', function (req, res) {
    console.log(req['files']['file'])
    if (!req.files)
        return res.status(400).send('No files were uploaded.');

    // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
    app.use(cors())
    let sampleFile = req['files']['file'];

    // Use the mv() method to place the file somewhere on your server
    sampleFile.mv('./uploads/' + req['files']['file']['name'], function (err) {
        if (err)
            return res.status(500).send(err);

        //res.redirect('http://127.0.0.1:3000/configuracion');
    });
    res.jsonp('success')
});


// Start server
app.listen(4000, function () {
    console.log("Node server running on http://localhost:4000");
});